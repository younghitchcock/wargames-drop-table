a = '    It was&nbsp;the best&nbsp;of times, it was the worst&nbsp;of&nbsp;times, it was the age&nbsp;of wisdom,&nbsp;it&nbsp;was&nbsp;the age of&nbsp;foolishness,&nbsp;it was&nbsp;the&nbsp;epoch&nbsp;of belief, it&nbsp;was&nbsp;the epoch of&nbsp;incredulity,&nbsp;it&nbsp;was the&nbsp;season of Light, it&nbsp;was&nbsp;the season of&nbsp;Darkness,&nbsp;it was the&nbsp;spring&nbsp;of hope,&nbsp;it&nbsp;was the winter of&nbsp;despair,&nbsp;we had everything before us, we&nbsp;had&nbsp;nothing before&nbsp;us,&nbsp;we&nbsp;were all going&nbsp;direct&nbsp;to Heaven,&nbsp;we&nbsp;were&nbsp;all&nbsp;going direct&nbsp;the&nbsp;other&nbsp;way - in&nbsp;short,&nbsp;the period&nbsp;was&nbsp;so far like the&nbsp;present&nbsp;period, that&nbsp;some&nbsp;of its&nbsp;noisiest&nbsp;authorities&nbsp;insisted&nbsp;on its&nbsp;being&nbsp;received,&nbsp;for good or for evil, in&nbsp;the&nbsp;superlative degree of&nbsp;comparison only.&nbsp;Decrypt!'

c = a.replace(" ", "0").replace("&nbsp;", "1")

print("A: {}\n\nCONVERT HIDDEN INFO TO BINARY: {}\n\n".format(a, c))

final = ''.join([x for x in c if x == '0' or x == '1'])

print("Extracted binary:  {}".format(final))

# Decode binary to ascii
n = int('0b'+final, 2)
lol = n.to_bytes((n.bit_length() + 7) // 8, 'big').decode()

print("Binary converted to ascii: {}".format(lol))