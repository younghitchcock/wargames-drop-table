from Crypto.Hash import HMAC
from Crypto.Hash import SHA256


m = "It was the best of times, it was the worst of times, it was the age of wisdom, it was the age of foolishness, it was the epoch of belief, it was the epoch of incredulity, it was the season of Light, it was the season of Darkness, it was the spring of hope, it was the winter of despair, we had everything before us, we had nothing before us, we were all going direct to Heaven, we were all going direct the other way - in short, the period was so far like the present period, that some of its noisiest authorities insisted on its being received, for good or for evil, in the superlative degree of comparison only.".encode("ascii")

#keys = [b' It was the best of times, it was the worst of times, it was the age of wisdom, it was the age of foolishness, it was the epoch of belief, it was the epoch of incredulity, it was the season of Light, it was the season of Darkness, it was the spring of hope, it was the winter of despair, we had everything before us, we had nothing before us, we were all going direct to Heaven, we were all going direct the other way - in short, the period was so far like the present period, that some of its noisiest authorities insist on its being received, for good or for evil, in the superlative degree of comparison only.']

keys = [b'Dickens', b'The Period', b'1859', b'A Tale Of Two Cities', b'a tale of two cities', b'ATaleOfTwoCities', b'ataleoftwocities', b'Look Deeper', b'LookDeeper', b'lookdeeper', b'HTML', b'in short, the period was so far like the present period, that some of its noisiest authorities insisted on its being received, for good or for evil, in the superlative degree of comparison only.']

for k in keys:

    hmac = HMAC.new(k, digestmod=SHA256)

    hmac.update(m)

    out = hmac.hexdigest()
    print(k)
    print(out)
    print()