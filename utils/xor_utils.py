def xor_bytes(m, k):
    c = []
    for i in range(len(m)):
        # For each letter in the message, we xor it with the value from the key
        # The key's value uses modulus to repeat the key and use the letters in the appropriate spot
        c.append(m[i] ^ k[i % len(k)])
    return c



def intarray_to_string(a):
    return ''.join([chr(x) for x in a])


