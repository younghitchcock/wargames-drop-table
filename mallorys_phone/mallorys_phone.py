from time import time
from Crypto.Hash import SHA256
import threading
import multiprocessing

def collide_thread(nonce_start, nonce_end, msg_collider, msg_hash, shortened_to, stop=None):
    print("Finding collisions against hash: {}. Starting at: {}. Ending at: {}".format(msg_hash, nonce_start, nonce_end))

    start_time = time()

    all_msg = [ msg_collider.format(x).encode("ascii")
               for x in range(nonce_start, nonce_end)]

    all_msg_hashed = [ SHA256.new(x).hexdigest()[:shortened_to] for x in all_msg ]

    print("Built hashes from {} to {}. Attempting collision".format(nonce_start, nonce_end))

    if msg_hash in all_msg_hashed:
        idx = all_msg_hashed.index(msg_hash)
        print("\n* * * * * * * * * * * * * * * * * * * * * * * * * * * * ")
        print("SUCCESS!!")
        print("Target Msg hash: {}.\nCollision message hash: {}\nCollision message text: {}".format(msg_hash, all_msg_hashed[idx], all_msg[idx]))
        print("Collision was found in: {}".format( time() - start_time ))
        print("* * * * * * * * * * * * * * * * * * * * * * * * * * * * \n")
    else:
        print( "\tFAIL: Hashes in nonce range [ {} {} ] did not collide".format(nonce_start, nonce_end) )

# ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

shortened_to = 7  # Collide with the first 28 bits....

# Set up message and modified message (m_dash)
msg = "Your friend Trent changed her phone number to 061752".encode('ascii')
msg_hash = SHA256.new(msg).hexdigest()[:shortened_to]

# All strings in Python 3 are Unicode by default - we need to convert them to ASCII for use in hash functions
msg_collider = "Your friend Young Hitchcock changed his phone number to 0{}"

# ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

hash_space_size = 2**27

nonces = [ int( (hash_space_size/multiprocessing.cpu_count()) * x ) for x in range(0, multiprocessing.cpu_count() + 1 )]

print("Using nonces: {}".format(nonces))

threads = []

for i in range( len(nonces)-1 ):
    n = nonces[i]
    n_end = nonces[i+1]
    t = threading.Thread(target=collide_thread, args=(n, n_end-1, msg_collider, msg_hash, shortened_to, nonces))
    t.start()
