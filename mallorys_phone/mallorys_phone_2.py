"""
PROBLEM:

Find a message that collides with the first 28 bits (7 hex characters) of the
SHA256 hash.

"""
from Crypto.Hash import SHA256
from multiprocessing import Pool

m = "Your friend Trent changed her phone number to 061752".encode("ascii")
h = "2d28f43"
m_dash = "Your friend Mallory changed her phone number to %s"

def find_collision(nonce):
    shortened_to = 7
    m_dash_new = m_dash % nonce
    m_dash_new = m_dash_new.encode("ascii")
    h_dash = SHA256.new(m_dash_new).hexdigest()[:shortened_to]

    # Print to track progress
    if nonce % 100000 == 0:
        print("Trying impersonator message: %s" % m_dash_new)
        print("With hash: %s" % h_dash)

    # If there is a collision, we have succeeded
    if h_dash == h:
        for i in range (1000):
            print("Successful impersonator message is: %s" % m_dash_new)

if __name__ == '__main__':
    # Distribute computation across pool of worker processes
    p = Pool(None)
    nonce_min = 0
    nonce_max = nonce_min + 1000000
    while True:
        p.map(find_collision, range(nonce_min, nonce_max))
        nonce_min = nonce_max + 1
        nonce_max += 1000000