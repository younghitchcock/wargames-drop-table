from time import time
from Crypto.Hash import SHA256

# ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

# Set up message and modified message (m_dash)
msg_a = "Melbourne Cup: $5,000 on Horse A (Secret Number) [transaction id: {}]"
msg_b = "Melbourne Cup: $5,000 on Horse B (Excess Knowledge) [transaction id: {}]"

# All strings in Python 3 are Unicode by default - we need to convert them to ASCII for use in hash functions
print("Finding collisions against ORIGINAL MESSAGE: %s" % msg_a)

# ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

# Each hex character represents 4 bits as 0-9A-F = 16 = 2 ** 4
shortened_to = 8  # Collide with the first 32 bits....
print("[ Attempting to match simplified %d-hex char hash (%d bits) ]" % (shortened_to, shortened_to * 4))

# ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

# Chosen plaintext attack

# { Choose the plaintext X }
# Generate a large number of variations of MSGB (the original)
# By formatting in many transaction ID numbers
# msga & msgb are pretty much interchangable for this task...
n_forged_messages = (2**21)
msgb_variations_txt = [msg_b.format(x).encode('ascii') for x in range(0, n_forged_messages)]
msg_b_hashes_shortened_list = [SHA256.new(x).hexdigest()[:shortened_to] for x in msgb_variations_txt]

print("Pre generated {} crack messages!\nEg: {}\n\n".format(n_forged_messages, msgb_variations_txt[0]))

# ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

start_time = time()
total_hashes = 0
nonce = 0
while True:

    # [ Msg A - the altered one we want to collide with Msg B ]

    # Vary the msb_a until its hash collides with msg_b's hash
    # Nonce is how msg_a's hash varies
    nonce += 1
    msg_a_varied = msg_a.format(nonce).encode("ascii")

    # * * *  Replace with HMAC implementation * * *
    # Create the suss hash and shorten it to match the current hash length
    h = SHA256.new(msg_a_varied)
    msg_a_hash_shortened = h.hexdigest()[:shortened_to]

    #  Collision attack - if any hashed msg_a collides with any varied original msg_b, we have a win.

    if msg_a_hash_shortened in msg_b_hashes_shortened_list:
        idx = msg_b_hashes_shortened_list.index(msg_a_hash_shortened)
        print("SUCCESS!")
        print( "msg A: \n\t{}\n\tHash = {}".format(msg_a_varied, msg_a_hash_shortened))
        print( "Collided with MsgB:\n\t{}\n\tHash = {}".format( msgb_variations_txt[idx], msg_b_hashes_shortened_list[idx] ))

        break

    # Else, loop around and try another variation
    total_hashes += len(msg_b_hashes_shortened_list)

    if nonce % 1000 == 0:
        print("\t\tTried {} hate message variations...".format(nonce))

end_time = time()



print("%d different message / hash combinations were tested in total over %0.2f seconds" % (total_hashes, end_time - start_time))
print("[ Brute Force ]\t\tExpecting %d attempts to find a collision => (2**(n-1)) [assuming no hash duplicates]" % 2 ** ((shortened_to * 4) - 1))
print("[ Extension ]\t\tExpecting %d attempts to find a collision => (2**(n/2)) [assuming no hash duplicates]" % 2 ** ( (shortened_to * 4) / 2))
print()
