from Crypto.Hash import SHA256

msga = "Melbourne Cup: $5,000 on Horse A (Secret Number) [transaction id: %d]".encode("ascii")
msgb = "Melbourne Cup: $5,000 on Horse B (Excess Knowledge) [transaction id: %d]".encode("ascii")

shortened_to = 8
nonce = 1
msgb_set = []

while True:
    msga_nonce = msga % nonce
    msgb_nonce = msgb % nonce
    a_hash = SHA256.new(msga_nonce).hexdigest()[:shortened_to]
    b_hash = SHA256.new(msgb_nonce).hexdigest()[:shortened_to]
    msgb_set.append((b_hash,nonce))

    match = False
    for b in msgb_set:
        h = b[0]
        if a_hash == h:
            print("msga nonce :",nonce)
            print("***")
            print("msgb nonce :",b[1])
            match = True
            break

    if match:
        break
    nonce += 1
    if nonce % 1000 == 0:
        print("Times Tried :",nonce)
