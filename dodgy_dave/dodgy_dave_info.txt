Dodgy Dave runs a betting agency. He sends out a hash the day before a big horse race to gullible betters.
After the race, he sends out the "original" bet he made the day before, which happens to have the winning horses.
The idea is that you give him lots of money and he promises to give you even more back. Surprise surprise, it's a scam.
He'll just disappear after taking lots of your money.

Prove to Dodgy Dave's clients that he's going to cheat them by showing how Dave's magic works.




# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

                            GOAL GOAL GOAL GOAL GOAL GOAL GOAL

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


Find a message `msga` that collides with the first 32 bits (8 hex characters) of the SHA256 hash of `msgb`.

You can put any string into the {} area as long as it only contains letters or numbers.

MSGA:  Melbourne Cup: $5,000 on Horse A (Secret Number) [transaction id: {}]

MSGB:  Melbourne Cup: $5,000 on Horse B (Excess Knowledge) [transaction id: {}]



* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

This just looks like a variation on the tute 1 exercise of finding collisions.

